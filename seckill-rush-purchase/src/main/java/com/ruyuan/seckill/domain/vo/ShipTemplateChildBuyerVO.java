package com.ruyuan.seckill.domain.vo;

import java.io.Serializable;

public class ShipTemplateChildBuyerVO extends ShipTemplateChildBaseVO implements Serializable {

    private String areaId;


    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
}
